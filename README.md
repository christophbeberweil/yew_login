# Yew Login

Demo applicaion to handle login, logout, state and some api endpoints in yew with yewdux.

## Access State in components

### Read State

### Write State

## Access State outside of components

### Read State

Obtain the token from AppState Store.

``` rust 
let app_state = dispatch::get::<AppState>();
print!("{}", app_state.token);
```
### Write State

Update the entries of the EntryStore Store.

``` rust
dispatch::reduce_mut::<EntryStore, _>(|entry_state| {
    entry_state.entries = vec![];
});
```