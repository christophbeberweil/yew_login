use yew::prelude::*;

use crate::types::Entry;

#[derive(PartialEq, Properties)]
pub struct EntryCardProps {
    pub entry: Entry,
}

#[function_component]
pub fn EntryCard(props: &EntryCardProps) -> Html {
    let EntryCardProps { entry } = props;

    //let entry = entry.clone();
    html! {
        <div class="p-2 rounded bg-indigo-200 hover:bg-indigo-300 duration-300 flex flex-col gap-1">
            <span class="font-bold text-ellipsis overflow-hidden">{ &entry.name }</span>
            <a class="text-ellipsis overflow-hidden text-indigo-500 rounded p-1 border border-transparent hover:border-indigo-500 duration-300" href={entry.url.clone()} target="_blank">{ &entry.url }</a>
            <span class="text-ellipsis overflow-hidden">{ &entry.notes }</span>
            <div class="flex flex-row gap-2 flex-wrap">{ entry.tags.clone().into_iter().map(|tag|{
                html!{
                    <span class="px-1 border border-indigo-600 rounded ">{{tag.name}}</span>
                }
            }).collect::<Html>() }
            </div>
            <span class="text-sm text-gray-500 text-ellipsis overflow-hidden">{ format!("{}", entry.id.0) }</span>
        </div>

    }
}
