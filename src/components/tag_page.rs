use crate::{
    api::get_tags,
    components::tag_or_loading::TagOrLoading,
    store::TagStore,
    types::{Tag, TagId},
};
use rand::{distributions::Alphanumeric, Rng};
use uuid::Uuid;
use yew::{function_component, html, Callback, Html, Properties};
use yewdux::prelude::use_store;

#[derive(PartialEq, Properties)]
pub struct TagPageProps {}

#[function_component]
pub fn TagPage(props: &TagPageProps) -> Html {
    let TagPageProps {} = props;

    let (tag_state, tag_state_dispatch) = use_store::<TagStore>();

    let onclick_add_random_tag = tag_state_dispatch.reduce_mut_callback(|tag_store| {
        tag_store.tags.push(Tag {
            id: TagId(Uuid::new_v4()),
            name: rand::thread_rng()
                .sample_iter(&Alphanumeric)
                .take(7)
                .map(char::from)
                .collect(),
        })
    });
    let onclick_refresh = Callback::from(move |_| {
        get_tags();
    });

    html! {
        <div class="flex flex-col gap-2">
            <h2 class="font-bold text-xl">{"Tags"}</h2>

            <p>{"Amount: "} {tag_state.tags.len()}</p>
            <div class="flex flex-row gap-2 flex-wrap">
                <button class="btn btn-secondary" onclick={onclick_add_random_tag}>{"add random"}</button>
                <button class="btn btn-secondary" onclick={onclick_refresh}>{"refresh "} <TagOrLoading/></button>
            </div>
            <div class="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6 gap-2">
                {
                    tag_state.tags.clone().into_iter().map(|tag| {
                        html!{<div class="p-2 rounded bg-indigo-200 hover:bg-indigo-300 duration-300 flex flex-col gap-1 ">
                            <span class="font-bold">{ tag.name }</span>
                            <span class="text-sm text-gray-500 text-ellipsis overflow-hidden">{ format!("{}", tag.id.0) }</span>
                        </div>}
                    }).collect::<Html>()
                }
            </div>

        </div>
    }
}
