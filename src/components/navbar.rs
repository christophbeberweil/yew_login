use crate::{
    store::{AppState, CounterState, EntryStore, TagStore}, components::{tag_or_loading::TagOrLoading, entry_or_loading::EntryOrLoading},
};
use yew::prelude::*;
use yewdux::prelude::*;

use crate::components::logout_button::LogoutButton;
#[derive(PartialEq, Properties)]
pub struct NavbarProps {}

#[function_component]
pub fn Navbar(props: &NavbarProps) -> Html {
    let NavbarProps {} = props;

    let (app_state, _app_state_dispatch) = use_store::<AppState>();
    let (counter_state, _app_state_dispatch) = use_store::<CounterState>();
    let (tag_state, _tag_state_dispatch) = use_store::<TagStore>();
    let (entry_state, _entry_state_dispatch) = use_store::<EntryStore>();

    html! {
        <nav class="bg-indigo-800 p-4 text-white flex flex-row gap-2 justify-between items-center">
            <a href="/" >
                <h1 class="text-xl font-bold">{"Yew Login Demo"}</h1>
            </a>

            <div class="text-sm flex flex-col gap-1">
                <p>{"Counter: "}{{counter_state.counter}}</p>
                <p>{"Token Length: "}{{format!("{:?}", app_state.token.clone().unwrap_or("".to_owned()).len())}}</p>
            </div>


            if app_state.token.is_some(){
                <div class="flex flex-row gap-2 flex-wrap">
                    <a class="btn btn-secondary" href="/entries"><EntryOrLoading/>{"Entries "}{entry_state.entries.len()}</a>
                    <a class="btn btn-secondary" href="/tags"><TagOrLoading/> {"Tags "} {tag_state.tags.len()}</a>
                </div>
                <LogoutButton/>
            }
        </nav>
    }
}
