use yew::prelude::*;
use yewdux::prelude::*;

use crate::{
    components::icons::{CogIcon, EntryIcon},
    store::EntryStore,
};

/// depending on the loading state of tag store, displays a tag or a loading icon
#[function_component]
pub fn EntryOrLoading() -> Html {
    let (entry_state, _entry_state_dispatch) = use_store::<EntryStore>();

    html! {
        <div>
            if entry_state.loading{
                <CogIcon class="w-4 h-4 animate-spin"/>
            } else {
                <EntryIcon class="w-4 h-4"/>
            }
        </div>
    }
}
