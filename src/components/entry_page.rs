use crate::{
    api::get_entries,
    components::entry_card::EntryCard,
    components::entry_or_loading::EntryOrLoading,
    store::EntryStore,
    types::{Entry, EntryId},
};
use rand::{distributions::Alphanumeric, Rng};
use uuid::Uuid;
use yew::{function_component, html, Callback, Html, Properties};
use yewdux::prelude::use_store;

#[derive(PartialEq, Properties)]
pub struct EntryPageProps {}

#[function_component]
pub fn EntryPage(props: &EntryPageProps) -> Html {
    let EntryPageProps {} = props;

    let (entry_state, entry_state_dispatch) = use_store::<EntryStore>();

    let onclick_add_random_entry = entry_state_dispatch.reduce_mut_callback(|entry_store| {
        entry_store.entries.push(Entry {
            id: EntryId(Uuid::new_v4()),
            name: rand::thread_rng()
                .sample_iter(&Alphanumeric)
                .take(7)
                .map(char::from)
                .collect(),
            notes: Some("jajaja".to_owned()),
            tags: vec![],
            url: "https://maper.de".to_owned(),
        })
    });

    let onclick_refresh = Callback::from(move |_| {
        get_entries();
    });

    html! {
        <div class="flex flex-col gap-2">
            <h2 class="font-bold text-xl">{"Entries"}</h2>

            <p>{"Amount: "} {entry_state.entries.len()}</p>
            <div class="flex flex-row gap-2 flex-wrap">
                <button class="btn btn-secondary" onclick={onclick_add_random_entry}>{"add random"}</button>
                <button class="btn btn-secondary" onclick={onclick_refresh}>{"refresh"}<EntryOrLoading/></button>
            </div>
            <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-2">
                {

                    entry_state.entries.clone().into_iter().map(|entry| {
                        html!{
                            <EntryCard entry={entry}/>
                        }
                    }).collect::<Html>()
                }
            </div>

        </div>
    }
}
