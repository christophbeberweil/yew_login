use crate::{api::perform_login, api_types::LoginData};

use std::ops::Deref;
use wasm_bindgen::JsCast;
use web_sys::EventTarget;
use web_sys::HtmlInputElement;
use yew::prelude::*;

use crate::components::counter::Counter;

#[derive(PartialEq, Properties, Clone)]
pub struct LoginFormProps {}

#[function_component]
pub fn LoginForm(props: &LoginFormProps) -> Html {
    let LoginFormProps {} = props;

    let state = use_state(LoginData::default);
    let error_state: Box<UseStateHandle<Option<String>>> = Box::new(use_state(|| None::<String>));

    // while more verbose, this works
    //callback that observes the username field and saves changes in the local state
    let username_input = Callback::from({
        let cloned_state = state.clone();
        move |e: InputEvent| {
            let target: Option<EventTarget> = e.target();
            // in order to use dyn_into, use wasm_bindgen::JsCast;
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
            if let Some(input) = input {
                let username = input.value();
                let mut data: LoginData = cloned_state.deref().clone();
                data.username = username;
                cloned_state.set(data);
            }
        }
    });

    //callback that observes the password field and saves changes in the local state
    let password_input = Callback::from({
        let cloned_state = state.clone();
        move |e: InputEvent| {
            let target: Option<EventTarget> = e.target();
            // in order to use dyn_into, use wasm_bindgen::JsCast;
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
            if let Some(input) = input {
                let password = input.value();
                let mut data: LoginData = cloned_state.deref().clone();
                data.password = password;
                cloned_state.set(data);
            }
        }
    });

    // event handler for login form submission
    // it reads the local state containing username and password and calls the perform_login function
    let onsubmit = {
        let cloned_state = state.clone();
        let cloned_error_state = error_state.clone();
        let _cloned_props = props.clone();
        Callback::from(move |event: SubmitEvent| {
            event.prevent_default();
            let data = cloned_state.deref().clone();

            perform_login(data, cloned_error_state.clone());
        })
    };

    // how to send the token to the parent???
    // write in global state?
    // emit a signal/callback/sth else?

    html! {
    <div class="flex flex-col gap-4 items-center">
        <div class="flex flex-col gap-2 rounded p-2 shadow">
                <h3 class="font-bold text-xl">{"Login"}</h3>
                <form class="contents" {onsubmit} >
                    <div class="flex flex-col gap-2">
                        <div>
                            <div class="flex justify-between">
                                <label for="titel" class="form-label">{"Username"}</label>
                            </div>
                            <div class="mt-1">
                                <input
                                     type="text" name="username"
                                    class="mt-1 p-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-900 focus:ring-indigo-900 text-sm"
                                    aria-describedby="username"
                                    placeholder="username"
                                    required=true
                                    oninput={username_input}

                                />
                            </div>
                        </div>
                        <div>
                            <div class="flex justify-between">
                                <label for="titel" class="form-label">{"Password"}</label>
                            </div>
                            <div class="mt-1">
                                <input type="password" name="password"
                                    class="mt-1 p-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-900 focus:ring-indigo-900 text-sm"
                                    aria-describedby="password"
                                    required=true
                                    oninput={password_input}

                                />
                            </div>
                        </div>
                    </div>

                    <div class="flex flex-row gap-2 ">
                        <button class="btn btn-primary" type="submit">{"Login"}</button>
                    </div>
                    <div>
                    <p>{"Username: "} {&state.username}</p>
                    <p>{"Pass: "} {&state.password.len()}</p>
                    </div>

                </form>


                <Counter/>
            </div>


            <div>

                {
                    match (*error_state).as_ref() {
                        Some(e) => {
                            html! {
                                <>
                                <p class="p-2 rounded bg-red-200 text-red-600">
                                    {e.to_string()}
                                </p>
                                </>
                            }
                        },
                        None => {
                            html! {
                                <>
                                </>
                            }
                        }
                    }
                }
                </div>
            </div>
    }
}
