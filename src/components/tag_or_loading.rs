use yew::prelude::*;
use yewdux::prelude::*;

use crate::{
    components::icons::{CogIcon, TagIcon},
    store::TagStore,
};

/// depending on the loading state of tag store, displays a tag or a loading icon
#[function_component]
pub fn TagOrLoading() -> Html {
    let (tag_state, _tag_state_dispatch) = use_store::<TagStore>();

    html! {
        <div>
            if tag_state.loading{
                <CogIcon class="w-4 h-4 animate-spin"/>
            } else {
                <TagIcon class="w-4 h-4"/>
            }
        </div>
    }
}
