use crate::components::navbar::Navbar;
use crate::components::{counter::Counter, login_form::LoginForm};
use crate::store::AppState;
use yew::prelude::*;
use yew_router::prelude::*;
use yewdux::prelude::*;

use super::entry_page::EntryPage;
use super::tag_page::TagPage;

#[derive(Clone, Routable, PartialEq)]
enum Route {
    #[at("/")]
    Home,
    #[at("/tags")]
    Tags,
    #[at("/entries")]
    Entries,
    #[not_found]
    #[at("/404")]
    NotFound,
}

fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! { <Counter/> },
        Route::Tags => html! {
        <TagPage/>
        },
        Route::Entries => html! {
            <EntryPage/>
        },
        Route::NotFound => html! { <p>{ "Nothing to see here... 🤷‍♂️" }</p> },
    }
}

#[derive(PartialEq, Properties, Default)]
pub struct AppProps {}

#[function_component]
pub fn App(props: &AppProps) -> Html {
    let AppProps {} = props;

    // use app_state to read the state
    // use app_state_dispatch to write to the state
    // a write operation triggers a re-rendering of the components displaying the state.
    let (app_state, _app_state_dispatch) = use_store::<AppState>();

    html! {
        <div>
            <div class="flex flex-col gap-4">
                <Navbar/>
                <div class="p-4 max-w-screen-xl mx-auto">
                    if app_state.token.is_some(){
                        // if we are logged in, show the router and its contents
                        <BrowserRouter>
                            <Switch<Route> render={switch} /> // <- must be child of <BrowserRouter>
                        </BrowserRouter>
                    } else {
                        <LoginForm/>
                    }
                </div>
            </div>
        </div>
    }
}
