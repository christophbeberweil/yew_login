use crate::api::perform_logout;
use yew::prelude::*;

#[derive(PartialEq, Properties)]
pub struct LogoutButtonProps {}

#[function_component]
pub fn LogoutButton(props: &LogoutButtonProps) -> Html {
    let LogoutButtonProps {} = props;

    let onclick = Callback::from(|_| {
        perform_logout();
    });

    html! {
        <button class="btn btn-primary" {onclick}>{"Logout"}</button>
    }
}
