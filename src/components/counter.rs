use yew::prelude::*;
use yewdux::prelude::*;

use crate::store::CounterState;

#[derive(PartialEq, Properties)]
pub struct CounterProps {}

#[function_component]
pub fn Counter(props: &CounterProps) -> Html {
    let CounterProps {} = props;

    let (counter_state, counter_state_dispatch) = use_store::<CounterState>();

    let onclick_increment =
        counter_state_dispatch.reduce_mut_callback(|app_state| app_state.counter += 1);
    let onclick_decrement =
        counter_state_dispatch.reduce_mut_callback(|app_state| app_state.counter -= 1);

    html! {
        <div class="flex flex-row gap-2 items-center">
            <button class="btn btn-secondary" onclick={onclick_decrement}>{"-1"}</button>

        <p>{{counter_state.counter}}</p>

        <button class="btn btn-secondary" onclick={onclick_increment}>{"+1"}</button>

        </div>
    }
}
