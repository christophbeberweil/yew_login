use std::rc::Rc;

use gloo_console::log;
use yewdux::prelude::Listener;

use crate::store::AppState;

/// implementation of a log listener. It fires whenever the store has changed.
/// We cannot use this pattern to react to a login event, such as a new token is saved
/// because if we do, the listener will fire even if other parts of the store have been changed, not only the token.
pub struct LogListener;
impl Listener for LogListener {
    type Store = AppState;

    fn on_change(&mut self, state: Rc<Self::Store>) {
        log!(format!("State changed to {:?}", state));
    }
}
pub struct LoginListener;
impl Listener for LoginListener {
    type Store = AppState;

    fn on_change(&mut self, state: Rc<Self::Store>) {
        if state.token.is_some() {
            log!("The token has changed, refresh all Stores!!!!");
        }

        if state.token.is_none() {
            log!("The is gone. Remove all Content from stores...");
        }
    }
}
