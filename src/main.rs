use components::app::App;

mod components;

mod api;
mod api_types;
mod listeners;
mod store;
mod types;

fn main() {
    yew::Renderer::<App>::new().render();
}
