use crate::{
    listeners::LogListener,
    types::{Entry, Tag},
};
use serde::{Deserialize, Serialize};
use yewdux::prelude::*;

#[derive(Debug, Default, Clone, PartialEq, Eq, Store, Serialize, Deserialize)]
#[store(storage = "local", storage_tab_sync, listener(LogListener))] // storage can also be "session"
pub struct AppState {
    pub token: Option<String>,
    pub backend_url: Option<String>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, Store, Serialize, Deserialize)]
#[store(storage = "local", storage_tab_sync, listener(LogListener))] // storage can also be "session"
pub struct CounterState {
    pub counter: i32,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, Store, Serialize, Deserialize)]
#[store(storage = "local", storage_tab_sync, listener(LogListener))] // storage can also be "session"
pub struct TagStore {
    pub tags: Vec<Tag>,
    pub loading: bool,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, Store, Serialize, Deserialize)]
#[store(storage = "local", storage_tab_sync, listener(LogListener))] // storage can also be "session"
pub struct EntryStore {
    pub entries: Vec<Entry>,
    pub loading: bool,
}
