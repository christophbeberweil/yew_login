use serde::{Deserialize, Serialize};


#[derive(Serialize, Deserialize, Default, Clone, Debug)]
pub struct LoginData {
    pub username: String,
    #[serde(rename(serialize = "cleartext_password"))]
    pub password: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct LoginResponse {
    pub token: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct NegativeLoginResponse {
    pub error: String,
    pub status: String,
}