use gloo_console::debug;
use reqwasm::{
    http::{Method, Request},
    Error,
};
use serde_json::json;
use yew::prelude::*;
use yewdux::dispatch::Dispatch;

use crate::{
    api_types::{LoginData, LoginResponse},
    store::{AppState, EntryStore, TagStore},
    types::{Entry, Tag},
};

/// deletes token and resets the stores
pub fn perform_logout() {
    let app_state_dispatch = Dispatch::<AppState>::new();
    let tag_store_dispatch = Dispatch::<TagStore>::new();
    let entry_store_dispatch = Dispatch::<EntryStore>::new();

    // remove token
    app_state_dispatch.reduce_mut(|app_state| app_state.token = None);
    // remove tags
    tag_store_dispatch.reduce_mut(|tag_state| {
        tag_state.tags = vec![];
        tag_state.loading = false;
    });
    // remove entries
    entry_store_dispatch.reduce_mut(|entry_store| {
        entry_store.entries = vec![];
    });
}

/// function that spawns a local future to perform the api request asynchronously
pub fn perform_login(login_data: LoginData, error_state: Box<UseStateHandle<Option<String>>>) {
    let app_state_dispatch = Dispatch::<AppState>::new();
    match Ok::<String, Error>(json!(login_data).to_string()) {
        Ok(login_data_in_js) => wasm_bindgen_futures::spawn_local(async move {
            let login_endpoint = "https://maper.node1337.de/api/auth/login".to_string();
            let fetched_result = Request::new(&login_endpoint)
                .method(Method::POST)
                .body(login_data_in_js)
                .header("Content-Type", "application/json")
                .send()
                .await;

            match fetched_result {
                Ok(response) => {
                    let parsed_login_successful_response: Result<LoginResponse, Error> =
                        response.json().await;

                    if let Ok(response) = parsed_login_successful_response {
                        // write to store
                        app_state_dispatch
                            .reduce_mut(|app_state| app_state.token = Some(response.token));

                        // refersh everything upon successful login
                        get_entries();
                        get_tags();
                    } else if let Err(e) = parsed_login_successful_response {
                        debug!(e.to_string());
                        error_state.set(Some(
                            "You could not be logged in with the provided credentials".to_owned(),
                        ));
                    }
                }
                Err(e) => {
                    error_state.set(Some(e.to_string()));
                }
            }
        }),
        Err(_e) => {}
    };
}

pub fn get_tags() {
    wasm_bindgen_futures::spawn_local(async move {
        let tag_store_dispatch = Dispatch::<TagStore>::new();
        let app_state_dispatch = Dispatch::<AppState>::new();

        tag_store_dispatch.reduce_mut(|tag_store| tag_store.loading = true);

        let endpoint = "https://maper.node1337.de/api/tags".to_string();

        // read from store
        let app_state = app_state_dispatch.get();
        let fetched_result = Request::new(&endpoint)
            .method(Method::GET)
            .header("Content-Type", "application/json")
            .header(
                "authorization",
                format!(
                    "Bearer {}",
                    app_state.token.clone().expect("there should be a token")
                )
                .as_str(),
            )
            .send()
            .await;

        match fetched_result {
            Ok(response) => {
                let parsed_login_successful_response: Result<Vec<Tag>, Error> =
                    response.json().await;

                if let Ok(tags) = parsed_login_successful_response {
                    tag_store_dispatch.reduce_mut(|tag_store| {
                        tag_store.tags = tags;
                        tag_store.loading = false;
                    });
                } else if let Err(e) = parsed_login_successful_response {
                    debug!(e.to_string());
                }
            }
            Err(e) => {
                debug!(e.to_string());
            }
        }
    });
}

pub fn get_entries() {
    wasm_bindgen_futures::spawn_local(async move {
        let entry_store_dispatch = Dispatch::<EntryStore>::new();
        let app_state_dispatch = Dispatch::<AppState>::new();
        entry_store_dispatch.reduce_mut(|entry_state| entry_state.loading = true);

        let endpoint = "https://maper.node1337.de/api/entries".to_string();

        // read from store
        let app_state = app_state_dispatch.get();

        let fetched_result = Request::new(&endpoint)
            .method(Method::GET)
            .header("Content-Type", "application/json")
            .header(
                "authorization",
                format!(
                    "Bearer {}",
                    app_state.token.clone().expect("there should be a token")
                )
                .as_str(),
            )
            .send()
            .await;

        match fetched_result {
            Ok(response) => {
                let parsed_response: Result<Vec<Entry>, Error> = response.json().await;

                if let Ok(entries) = parsed_response {
                    // write to store
                    entry_store_dispatch.reduce_mut(|entry_state| {
                        entry_state.entries = entries;
                        entry_state.loading = false;
                    });
                } else if let Err(e) = parsed_response {
                    debug!(e.to_string());
                }
            }
            Err(e) => {
                debug!(e.to_string());
            }
        }
    });
}
