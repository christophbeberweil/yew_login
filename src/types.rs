use serde::{Deserialize, Serialize};
use uuid::Uuid;


#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq, Hash)]
pub struct Tag {
    pub id: TagId,
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
pub struct TagId(pub Uuid);

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
pub struct EntryId(pub Uuid);

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq, Hash)]
pub struct Entry {
    pub id: EntryId,
    pub name: String,
    pub url: String,
    pub notes: Option<String>,
    #[serde(default = "default_tags")]
    pub tags: Vec<Tag>,
}

fn default_tags() -> Vec<Tag> {
    vec![]
}
