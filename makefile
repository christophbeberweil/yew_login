serve:
	trunk serve


build_css:
	./tailwindcss -i styles/input.css -o styles/output.css --watch

build_css_prod:
	./tailwindcss -i styles/input.css -o styles/output.css --minify